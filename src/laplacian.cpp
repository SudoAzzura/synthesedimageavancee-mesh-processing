
#include "laplacian.h"
#include "mesh.h"
#include <Eigen/SparseCholesky>

using namespace Eigen;
using namespace pmp;

typedef SparseMatrix<float> SpMat;
typedef PermutationMatrix<Dynamic> Permutation;

double cotan_weight(const SurfaceMesh &mesh, pmp::Halfedge he) {
  auto points = mesh.get_vertex_property<Point>("v:point");
  Halfedge hnext = mesh.prev_halfedge(he);
  Halfedge hnextopp = mesh.next_halfedge(mesh.opposite_halfedge(he));

  Halfedge hoppnextopp = mesh.opposite_halfedge(hnextopp);
  Halfedge hprevopp = mesh.prev_halfedge(mesh.opposite_halfedge(he));

  Halfedge hoppnext = mesh.opposite_halfedge(mesh.next_halfedge(he));
  Halfedge hoppprev = mesh.opposite_halfedge(mesh.prev_halfedge(he));

  Point dist2p0 = points[mesh.to_vertex(hoppprev)];
  Point dist3p0 = points[mesh.to_vertex(hoppnextopp)];
  Point dist1p2 = points[mesh.to_vertex(hoppnext)];
  Point dist4p2 = points[mesh.to_vertex(hprevopp)];
  Point dist2p1 = points[mesh.from_vertex(hoppprev)];
  Point dist1p1 = points[mesh.from_vertex(hoppnext)];
  Point dist3p3 = points[mesh.from_vertex(hoppnextopp)];
  Point dist4p3 = points[mesh.from_vertex(hprevopp)];

  Vector3f dist1 = normalize(dist1p1 - dist1p2);

  Vector3f dist2 = normalize(dist2p1 - dist2p0);

  Vector3f dist3 = normalize(dist3p0 - dist3p3);
  Vector3f dist4 = normalize(dist4p2 - dist4p3);

  auto cot_a = dist2.dot(dist1) / dist2.cross(dist1).norm();
  auto cot_b = dist3.dot(dist4) / dist3.cross(dist4).norm();
  //std::cout << "cot: " << dist4    << std::endl;
  
  return (cot_a + cot_b)/2.0 ;
}
     

/// Computes the Laplacian matrix in matrix \a L using cotangent weights or the
/// graph Laplacian if useCotWeights==false.
void create_laplacian_matrix(const SurfaceMesh &mesh, SpMat &L,
                             bool useCotWeights) {
  // number of vertices in mesh
  int n = mesh.n_vertices();
  typedef Eigen::Triplet<double> T;
  std::vector<T> tripletList;
  std::size_t s = 3*n*sizeof(double);
  tripletList.reserve(s);
  double weight = 0;

  /* On parcours tous les vertex de la mesh, l'Echelle de notre mesh*/
  for(auto v: mesh.vertices()) {
    Halfedge h0 =  mesh.halfedge(v);
    Halfedge htmp = h0;
    Vertex vtmp = v;
    int nb_voisin = 0;
    /* graph Laplacian */
    weight =1;
    
    do {/* Echelle d'un voisinage*/ 
      do {/*Echelle d'une face*/
        if(useCotWeights) {
          weight += cotan_weight(mesh,htmp);
        }
        nb_voisin += 1;
        htmp = mesh.next_halfedge(htmp);
        vtmp = mesh.to_vertex(htmp);
        
        /*
          On ajoute 1 là où il y a une liaison
        */
        tripletList.push_back(T(v.idx(),vtmp.idx(),1));
      /*
        tant qu'on a pas atteint de le vertex de départ
      c a d tant qu'on a par parcouru tous les sommets de la face
      */
      }while(v != vtmp);
      /*
        notre halfedge courant devient l'opposé du dernier de la face précédente
      */
      htmp = mesh.opposite_halfedge(htmp);
    /*
    tant qu'on n'atteint pas le halfedge du début
    c a d tant qu'on a pas parcouru tous les voisins de v
    on continue de parcourir les voisins
    */
    }while(h0!= htmp);
    /* 
      On ajoute le nombre de voisin lorsque l'on a fini de les parcourir
      au coordonnée i==j
      */
    tripletList.push_back(T(v.idx(),v.idx(),-nb_voisin));
  }
  L.setFromTriplets(tripletList.begin(), tripletList.end());
}


/// Computes the permutation putting selected vertices (mask==1) first, and the
/// others at the end. It returns the number of selected vertices.
int create_permutation(const SurfaceMesh &mesh, Permutation &perm) {
  auto masks = mesh.get_vertex_property<int>("v:mask");

  // number of vertices in mesh
  int n = mesh.n_vertices();

  perm.resize(n);
  
  int i=0,j=n-1;
  for(auto v:mesh.vertices()) {
    if(masks[v] == 1) {
      //on ajoute pour les colones pas pour les lignes
      perm.indices()[v.idx()] = i;
      i++;
    } else {
      perm.indices()[v.idx()] = j;
      j--;
    }
  }
  return i;
}

/// Performs the poly-harmonic interpolation (order k) over the selected
/// vertices (mask==1) of the vertex attributes u. For each vertex V of index i,
///     if  mask[V]!=1 then u.col(i) is used as input constraints,
///     otherwise, mask[V}==1, and u.col(i) is replaced by the poly-harmonic
///     interpolation of the fixed values.
void poly_harmonic_interpolation(const SurfaceMesh &mesh, Ref<MatrixXf> u,
                                 int k) {
  // Number of vertices in the mesh
  int n = mesh.n_vertices();

  // 1 - Create the sparse Laplacian matrix
  SpMat L(n,n);
  create_laplacian_matrix(mesh, L, true);

  // 2 - Create the permutation matrix putting the fixed values at the end,
  //     and the true unknown at the beginning
  Permutation perm;
  int nb_unknowns = create_permutation(mesh, perm);

  // 3 - Apply the permutation to both rows (equations) and columns (unknowns),
  //     i.e., L = P * L * P^-1
  if(k==2) {
    L = L*L;
  }
  if(k==3) {
    L= L*L*L;
  }

  L = perm*L*perm.inverse();
  MatrixXf utmp = perm * u.transpose() ;
  SpMat L00 = L.topLeftCorner(nb_unknowns,nb_unknowns);
  SpMat L01 = L.topRightCorner(nb_unknowns,n-nb_unknowns);

  // 4 - solve L * [x^T u^T]^T = 0, i.e., L00 x = - L01 * u
  
  Eigen::SimplicialLDLT<Eigen::SparseMatrix<float> > solver;
  solver.compute(L00);
  utmp.topRows(nb_unknowns) =solver.solve(-L01 * utmp.bottomRows(n-nb_unknowns));
  
  if(solver.info()!=Success) {
    // solving failed
    std::cout << "solving failed" << std::endl;
    return;
  }
  //std::cout << "solving succeded" << std::endl;

  utmp =  perm.inverse() *utmp;
  // 5 - Copy back the results to u
  u =  utmp.transpose() ;

}

void harmonic_deformation(const SurfaceMesh &mesh, Ref<MatrixXf> u,pmp::Vertex vectselected) {

                                 }
