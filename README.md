# TD2 - Interpolation et déformation poly-harmonique - Chloé Neuville

## 2. Création de la matrice Laplacienne : Fonctionnelle
La création de la Laplacienne dans la fonction create_laplacian_matrix,
n'est pas exactement comme vous le souhaitiez, au lieu de faire opposite puis next jusqu'à atteindre l'halfedge de départ. Je parcours les faces de chaque vertex et y récupère les sommets. Je sais faire la fonction que vous vouliez, je l'ai même faite dans ce commit: 0e6649d301c640e207c96f347f58f0de72533394 lorsque je pensais qu'un problème venait de ma fonction .
J'ai préféré garder ma fonction car elle est unique et fonctionnelle.

Ce qui était perturbant c'est que cette matrice laplacienne n'est pas ordinaire car la matrice de permutation de base est négative là où on signifie qu'il y a une adjacence et positive au niveau des degrés, l'inverse de la notre.

Voici un problème qui m'a poursuivi un certain temps dû simplement au fait que je ne comptais pas le nombre de voisin au bon moment :


![alt text](./imgForMd/LapinPyramidezarb.png)


Voilà ce que ça donne un lapin transformé avec un voisin en moins concidéré.
## 3. Isolation des inconnues (ré-ordonnancement) : Fonctionnelle
La matrice de permutation était un petit plaisir à coder, elle m'a surprise dans sa complexité apparente qui devient simple une fois posée sur papier.
J'ai eu droit à de belle explosion de lapin avant d'avoir le bon résultat.


![alt text](./imgForMd/lapinExplose.png)


Ce qui fut le plus difficile

## 4. Résolution du système : Fonctionnel
Donc là, je me retrouve avec les lignes de ma laplacienne placées au dessus de la matrice en la multipliant par la matrice de permutation inverse puis par la matrice de permutation.
Je transpose ma matrice u et la multiplie par la matrice de permutation.
Je résouds le système en récupérant le haut droit et le haut gauche de la matrice pour résoudre cette équation :
L00U=−L01Ubarre
avec le solveur. Puis je multiplie ma nouvelle matrice u par la matrice de permutation pour la remettre dans le bon ordre puis je la transpose, pour la mettre dans le bon sens.

Et hop un Lapin Lissé avec cot = 1 et k = 1.


![alt text](./imgForMd/lapincot1k1lisse1.png)
![alt text](./imgForMd/lapincot1k1lisse2.png)
![alt text](./imgForMd/lapincot1k1lisse3.png)
![alt text](./imgForMd/lapincot1k1lisse4.png)
![alt text](./imgForMd/lapincot1k1lisse5.png)
![alt text](./imgForMd/lapincot1k1lisse6.png)

## 5. Reconstruction bi-harmonique et tri-harmonique :Fonctionnelle
Pour la Bi-harmonique il suffit de multiplier la laplacienne par elle même, pour la tri-harmonique il faut faire comme la bi mais en multipliant encore une fois la laplacienne.

### Bi-harmonique et cot = 1 :
![alt text](./imgForMd/lapincot1k2lisse1.png)

![alt text](./imgForMd/lapincot1k2lisse2.png)

remplissage des trous :
![alt text](./imgForMd/lapincot1k2lisse3.png)

### Tri-harmonique et cot = 1 :
![alt text](./imgForMd/lapincot1k3lisse1.png)

![alt text](./imgForMd/lapincot1k3lisse2.png)

remplissage des trous :
![alt text](./imgForMd/lapincot1k3lisse3.png)

### Couleurs

![alt text](./imgForMd/couleur1.png)

![alt text](./imgForMd/couleur2.png)

## 6. Formule des co-tangentes : Fonctionnelle
create_laplacian_matrix(mesh, L, true);
Utilisation de cotan_weight(const SurfaceMesh &mesh, pmp::Halfedge he)
Pour cette partie il a fallut que je réétudie les angles et quels étaient ces fameux angles Alpha et Beta, donc les deux angles opposés à l'halfedge. De plus, les calculs devaient être fait à partir des bons vecteurs et dans le bon sens sinon les résultats étaient vraiment mauvais et faisait exploser le lapin ou lui faisait des transformations non satisfaisantes au niveau des trous.

![alt text](./imgForMd/lapincotbon1.png)

![alt text](./imgForMd/lapincotbon2.png)

![alt text](./imgForMd/lapincotbon3.png)

![alt text](./imgForMd/lapincotbon4.png)

![alt text](./imgForMd/lapincotbon5.png)
